package com.example.androidhive;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class AndroidDownloadFileByProgressBarActivity extends Activity {

	Button btnShowProgress, play;
	String LOG_TAG = "Download poc";
	// Progress Dialog
	private ProgressDialog pDialog;
	Context ctx;
	final String methodName = "GET";
	final String KEY = "AES";
	final String dialogMessage = "Downloading file. Please wait...";
	ImageView my_image;
	// File url to download
	private static String file_url = "http://api.androidhive.info/progressdialog/hive.jpg";
//	private static String url1 = "http://192.168.1.135:88/Sandeep/waras.mp3";
	private static String url1 = "https://s3.amazonaws.com/sandeepmedia/gostories/waras.mp3";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ctx = this;
		// show progress bar button
		btnShowProgress = (Button) findViewById(R.id.btnProgressBar);
		play = (Button) findViewById(R.id.play);
		// Image view to show image after downloading
		my_image = (ImageView) findViewById(R.id.my_image);
		/**
		 * Show Progress bar click event
		 * */
		btnShowProgress.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					new DownloadFileFromURL().execute();
				}
				catch (Exception e){
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					String msg = sw.toString();
					Log.e("exception  " , e.getMessage() + "error " + msg);
				}
			}
		});
		play.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					decrypt();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	void decrypt() throws IOException, NoSuchAlgorithmException,NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

		File extStore = Environment.getExternalStorageDirectory();
		FileInputStream fis = new FileInputStream(extStore+ "/download" + "/test");
		//	FileOutputStream fos = new FileOutputStream(extStore + "/decrypted");
		SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(),KEY);
		Cipher cipher = Cipher.getInstance(KEY);
		cipher.init(Cipher.DECRYPT_MODE, sks);
		CipherInputStream cis = new CipherInputStream(fis, cipher);
		int b;
		byte[] d = new byte[1024*8];
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		while ((b = cis.read(d)) != -1) {
			os.write(d, 0, b);
		}
		byte[] decrypted = os.toByteArray();
		//	Log.d("decrypted ", "decrypted    " + decrypted);
//		fos.flush();
//		fos.close();
		os.close();
		cis.close();
		playMp3(decrypted);
	}
	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, byte[]> {
		/**
		 * Before starting background thread
		 * Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//onCreateDialog(progress_bar_type);
			pDialog = new ProgressDialog(AndroidDownloadFileByProgressBarActivity.this);
			pDialog.setMessage(dialogMessage);
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected byte[] doInBackground(String... f_url) {

			byte[] inarry = null;
			try {
				URL url = null;
				Log.d("url : ",url1);
				url = new URL(url1);
				HttpURLConnection c = null;
				c = (HttpURLConnection) url.openConnection();
				Log.d("connection  : ", String.valueOf(c));
				c.setRequestMethod(methodName);
				c.setDoOutput(true);
				c.connect();
				//c.setDoOutput(true);
//				c.setDoInput(true);
				int lenghtOfFile = c.getContentLength();
				Log.d("lenghtOfFile  : ", String.valueOf(lenghtOfFile));
				InputStream is = c.getInputStream();
				Log.d("is  : ", String.valueOf(is));
/*				String PATH = Environment.getExternalStorageDirectory()
					+ "/download/";
				Log.d(LOG_TAG, "PATH: " + PATH);
				File file = new File(PATH);
				file.mkdirs();
				String fileName = "test1.mp3";
				File outputFile = new File(file, fileName);
				FileOutputStream fos = null;
				fos = new FileOutputStream(outputFile);*/
				byte[] buffer = new byte[1024*8];
				Log.d("buffer  : ", String.valueOf(buffer));
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				int len1 = 0;
				long total = 0;
				while ((len1 = is.read(buffer)) != -1) {
					total += len1;
					try {
						output.write(buffer, 0, len1);
//						fos.write(buffer, 0, len1);
					} catch (Exception e) {
						e.printStackTrace();
					}
					publishProgress("" + (int) ((total * 100)/lenghtOfFile));
				}
				inarry  = output.toByteArray();
				Log.d("inarry  : ", String.valueOf(inarry));
				is.close();
				output.close();
//				fos.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				String msg = sw.toString();
				Log.e("exception  " , e.getMessage() + "error " + msg);
			}
			return inarry;
		}
		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}
		/**
		 * After completing background task
		 * Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(byte[] inarray) {
			try {
				String PATH = Environment.getExternalStorageDirectory()
						+ "/download/";
				Log.d(LOG_TAG, "PATH: " + PATH);
				File file = new File(PATH);
				file.mkdirs();
				String fileName = "test";
				File outputFile = new File(file, fileName);
				FileOutputStream fos = null;
				fos = new FileOutputStream(outputFile);
				SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(),KEY);
				Cipher cipher = Cipher.getInstance(KEY);
				cipher.init(Cipher.ENCRYPT_MODE, sks);
				CipherOutputStream cos = new CipherOutputStream(fos, cipher);
				cos.write(inarray);
				cos.flush();
				cos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			pDialog.dismiss();
		}
	}
	private void playMp3(byte[] mp3SoundByteArray) {
		try {
			File tempMp3 = File.createTempFile("kurchina", "mp3", getCacheDir());
			tempMp3.deleteOnExit();
			FileOutputStream fos = new FileOutputStream(tempMp3);
			fos.write(mp3SoundByteArray);
			fos.close();
			MediaPlayer mediaPlayer = new MediaPlayer();
			FileInputStream fis = new FileInputStream(tempMp3);
			mediaPlayer.setDataSource(fis.getFD());
			mediaPlayer.prepare();
			mediaPlayer.start();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}